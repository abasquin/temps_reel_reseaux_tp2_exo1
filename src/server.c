#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>

#define NB_CLIENT_MAX 10

int sockfd;
int csockfd;
int lsockfd;
int csockfd;

int current_client_id;
int list_csockfd[NB_CLIENT_MAX] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

#define MY_PORT 2031
//on peut rajouter 1 deuxième thread, 1 pour criture, l'autre pour lecture
//si client
void *transit(void *arg)
{
	int current_client = (int)arg;
	printf("je suis le thread ecriture  %d de salon\n", current_client);
	int c, ret, i;

	while(1)
	{
		ret = read(list_csockfd[current_client], &c, sizeof(c));
		if(ret<=0)
		{
			printf("erreur read thread %d \n", current_client);
			list_csockfd[current_client] = -1;
			close(list_csockfd[current_client]);
			return NULL;
			//break;
		}
		else
		{
			//printf("on a lu %c", (char) c);
			for (i=0;i<NB_CLIENT_MAX;i++)
			{
				if ((list_csockfd[i] != -1) && (i !=current_client))
				{
					//printf("ecriture\n");
					ret=write(list_csockfd[i], &c, sizeof(c));

					if(ret<0)
					{
						printf("erreur write\n");
						break;
					}
				}
			}
		}
	}
	printf("on sort du while");
}




int main(int argc, char *argv[])
{
	int ret, c, i;
	struct sockaddr_in serv_addr;
	bool addable_client = false;

	pthread_t thread_client[10];//thread[20]
	bool isValid;

	/*On vérifie que le nombre d'argument est bon
	if(argc != 2)
	{
		printf("erreur d'utilisation, usage : ./client ip_addr\n");
		return -1;
	}*/


	/*On crée la socket 1*/
	lsockfd=socket(AF_INET, SOCK_STREAM, 0);
	if(lsockfd<0)
	{
		printf("erreur socket\n");
		return -1;
	}


	/*On crée les paramètres réseaux*/
	//relier adresse et socket : bind
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(MY_PORT);
	serv_addr.sin_addr.s_addr = htons(INADDR_ANY);//on authorise tt aux connections

	ret = bind(lsockfd, (struct sock_addr *)&serv_addr, sizeof(struct sockaddr));
	if(ret<0)
	{
		printf("erreur bind\n");
		return -1;
	}


	ret = listen(lsockfd, 1);/* 1 = nombre max de liaison, on écoute les connexions entrantes*/
	/*le programe va tuorner jsqu'a accept pour accepter la connexion avec un client*/
	if(ret<0)
	{
		printf("erreur listen\n");
		return -1;
	}

	while(1)
	{
		current_client_id = 0;
		addable_client = false;
		int new_id;

		for (current_client_id=0; current_client_id<NB_CLIENT_MAX; current_client_id++)
		{

			if (list_csockfd[current_client_id] == -1)
			{
				addable_client = true;
				break;
			}
		}
		if (addable_client)
		{
			new_id = accept(lsockfd, (struct sockaddr *)NULL, NULL);

			if(new_id<0)
			{
				printf("erreur accept\n");
			}
			else
			{
				list_csockfd[current_client_id] = new_id;
				if (pthread_create(&thread_client[current_client_id], NULL, transit, (void*)current_client_id))//hread[i], transit
				{
					isValid = false;
				}
			}
		}
	}
	return 0;
}
