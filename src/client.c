#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>

#define MY_PORT 2031

int sockfd;
int csockfd;


void *fctLecture(void *arg)
{
	int ret, c;
	printf("je suis le thread lecture de client\n");
	while (1)
	{
		ret=read(sockfd, &c, sizeof(c));
		if(ret<=0)
		{
			printf("erreur read\n");
			break;
		}
		printf("%c", (char) c);
	}
}



int main(int argc, char *argv[])
{
	int ret, c;
	struct sockaddr_in serv_addr;
	pthread_t thread_lecture;
	bool isValid;

	/*On vérifie que le nombre d'argument est bon*/
	if(argc != 2)
	{
		printf("erreur d'utilisation, usage : ./client ip_addr\n");
		return -1;
	}


	/*On crée la socket*/
	sockfd=socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd<0)
	{
		printf("erreur socket\n");
		return -1;
	}


	/*On crée les paramètres réseaux*/
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(MY_PORT);
	ret = inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);
	if(ret<0)
	{
		printf("erreur inet_pton\n");
		return -1;
	}


	/*On se connecte*/
	ret=connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	if(ret <0)
	{
		printf("erreur connect\n");
		return -1;
	}


	if (pthread_create(&thread_lecture, NULL, fctLecture, NULL))
	{
		isValid = false;
		//printf("le thread d'aquisition du clavier n'a pas été créé\n");
	}



	/*write sur le descripteur de socket*/
	while(1)
	{
		c = getchar();
		if(c<0)
		{
			printf("erreur getchar\n");
			return -1;
		}

		ret=write(sockfd, &c, sizeof(c));
		if(ret<=0)
		{
			printf("erreur write\n");
			return -1;
		}
	}
	return 0;
}
